package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Triangulo extends Forma {
	public Triangulo(List<Double> lados) {
		super(lados);
	}

	private double ladoA = super.lados.get(0);
	private double ladoB = super.lados.get(1);
	private double ladoC = super.lados.get(2);

	@Override
	public double calculaArea() {
		if ((ladoA + ladoB) > ladoC && (ladoA + ladoC) > ladoB && (ladoB + ladoC) > ladoA) {
			double s = (ladoA + ladoB + ladoC) / 2;
			double area = Math.sqrt(s * (s - ladoA) * (s - ladoB) * (s - ladoC));
			return area;

		} else {
			System.out.println("Mas o triangulo informado era inválido :/");
			return 0;
		}

	}

}
