package br.com.mastertech.itau.imersivo.formas;

import java.util.List;

public class Quadrado extends Forma{

	public Quadrado(List<Double> lados) {
		super(lados);
	}

	@Override
	public double calculaArea() {
		return (super.lados.get(0) * super.lados.get(0));
	}

}
