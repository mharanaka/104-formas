package br.com.mastertech.itau.imersivo.formas;

import java.util.ArrayList;
import java.util.List;

public abstract class Forma {

	public Forma(List<Double> lados) {
		this.lados = lados;
	}

	protected List<Double> lados = new ArrayList<>();

	public List<Double> getLados() {
		return lados;
	}

	public void setLados(List<Double> lados) {
		this.lados = lados;
	}

	public abstract double calculaArea();

}
